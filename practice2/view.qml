import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0


Page {
    width: 640
    height: 480

    header: Label {
        color: "#15af15"
        text: qsTr("System Cleaner")
        font.pointSize: 17
        font.bold: true
        font.family: "Arial"
        renderType: Text.NativeRendering
        horizontalAlignment: Text.AlignHCenter
        padding: 10
    }

    Rectangle {
        id: root
        width: parent.width
        height: parent.height


        Grid {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                columns: 4
                spacing: 10

                Rectangle { id:rect; color: "#aa6666"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1;}
                Rectangle { color: "#aaaa66"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
                Rectangle { color: "#9999aa"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
                Rectangle { color: "#6666aa"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
                Rectangle { color: "#aa6666"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
                Rectangle { color: "#aaaa66"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
                Rectangle { color: "#9999aa"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
                Rectangle { color: "#6666aa"; width: 150; height: 150; border.color: "black"; border.width: 1; radius: 1 }
            }
    }
}
